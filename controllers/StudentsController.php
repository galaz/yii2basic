<?php
	namespace app\controllers;
	use Yii;
	use app\models\Students;
	use yii\web\Controller;

	class StudentsController extends Controller
	{		
	    public function actionIndex()
	    {
	    	$student = students::find()->one();
	        return $this->render('index', ['student' => $student]);
	    }

	    public function actionShow()
	    {
	    	$student = students::find()->all();
	        return $this->render('show', ['student' => $student]);
	    }
	}
?>