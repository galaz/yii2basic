<?php

namespace app\models;
use yii\db\ActiveRecord;
use yii;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    
    public static function tableName(){
        return 'user';// חיבור לטבלת יוזר 
    }
   
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id); // בגלל הקישור של הטבלה ליוזר בפונקציה הקודמת נדע לאן ללכת
    }
    
    public function rules()
    {
        return
        [
            [['username', 'password', 'auth_Key'],'string', 'max' =>255],
            [['username', 'password'],'required'],
            [['username'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('Not supported');

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username'=>$username]);// בתוך הסוגריים נכתוב את התנאי שדרכו נמצא את האובייקט הרלוונטי
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_Key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($auth_Key)
    {
        return $this->auth_Key === $auth_Key;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
  
  
  //test hashed password

    public function validatePassword($password)
    {
        return $this->isCorrectHash($password, $this->password);
    }
    
    private function isCorrectHash($plaintext, $hash)
    {
        return Yii::$app->security->validatePassword($plaintext, $hash);
    }
  
  
  
    //hash password before saving
    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
                    generatePasswordHash($this->password);

        if ($this->isNewRecord)
            $this->auth_Key = Yii::$app->security->generateRandomString(32);

        return $return;
    }

  
}
