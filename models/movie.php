<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "movie".
 *
 * @property integer $id
 * @property string $name
 * @property string $genre
 * @property integer $age
 * @property integer $score
 */
class movie extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'movie';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'genre', 'age', 'score'], 'required'],
            [['age', 'score'], 'integer'],
            [['name', 'genre'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'genre' => 'Genre',
            'age' => 'Age',
            'score' => 'Score',
        ];
    }
}
