<?php

use yii\db\Migration;

/**
 * Handles adding description to table `customers`.
 */
class m170522_174625_add_description_column_to_customers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('customers', 'description', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('customers', 'description');
    }
}
