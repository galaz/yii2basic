<?php

use yii\db\Migration;

class m170612_193900_user extends Migration
{

        public function up()
    {
       $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
            'auth_Key' => $this->string()->notNull(),
            
        ]);
    }

    public function down()
    {
       $this->dropTable('user');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
