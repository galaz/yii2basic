<?php

use yii\db\Migration;

class m170612_195102_movie extends Migration
{
    public function up()
    {
         $this->createTable('movie', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'genre' => $this->string()->notNull(),
            'age' => $this->integer()->notNull(),
            'score' => $this->integer()->notNull(),
               ]);

    }

    public function down()
    {
        $this->dropTable('movie');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
