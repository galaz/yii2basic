<?php

use yii\db\Migration;

class m170529_080814_student extends Migration
{

    public function up()
    {
		     $this->createTable('student', [
             'id' => $this->primaryKey(),
			 'name' => $this->string()->notNull(),
			 'id_num' => $this->string()->notNull(),
			 'age' => $this->string()->notNull(),

        ]);
		

    }

    public function down()
    {
		$this->dropTable('student');
        
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
